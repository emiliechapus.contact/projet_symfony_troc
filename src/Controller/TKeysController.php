<?php

namespace App\Controller;

use App\Entity\TKeys;
use App\Form\TKeysType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/t/keys")
 */
class TKeysController extends AbstractController
{
    /**
     * @Route("/", name="t_keys_index", methods={"GET"})
     */
    public function index(EntityManagerInterface $entityManager): Response
    {
        $tKeys = $entityManager
            ->getRepository(TKeys::class)
            ->findAll();

        return $this->render('t_keys/index.html.twig', [
            't_keys' => $tKeys,
        ]);
    }

    /**
     * @Route("/new", name="t_keys_new", methods={"GET", "POST"})
     */
    public function new(Request $request, EntityManagerInterface $entityManager): Response
    {
        $tKey = new TKeys();
        $form = $this->createForm(TKeysType::class, $tKey);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($tKey);
            $entityManager->flush();

            return $this->redirectToRoute('t_keys_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('t_keys/new.html.twig', [
            't_key' => $tKey,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{keysId}", name="t_keys_show", methods={"GET"})
     */
    public function show(TKeys $tKey): Response
    {
        return $this->render('t_keys/show.html.twig', [
            't_key' => $tKey,
        ]);
    }

    /**
     * @Route("/{keysId}/edit", name="t_keys_edit", methods={"GET", "POST"})
     */
    public function edit(Request $request, TKeys $tKey, EntityManagerInterface $entityManager): Response
    {
        $form = $this->createForm(TKeysType::class, $tKey);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('t_keys_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('t_keys/edit.html.twig', [
            't_key' => $tKey,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{keysId}", name="t_keys_delete", methods={"POST"})
     */
    public function delete(Request $request, TKeys $tKey, EntityManagerInterface $entityManager): Response
    {
        if ($this->isCsrfTokenValid('delete'.$tKey->getKeysId(), $request->request->get('_token'))) {
            $entityManager->remove($tKey);
            $entityManager->flush();
        }

        return $this->redirectToRoute('t_keys_index', [], Response::HTTP_SEE_OTHER);
    }
}
