<?php

namespace App\Controller;

use App\Entity\TCompteUtilisateur;
use App\Form\TCompteUtilisateurType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/t/compte/utilisateur")
 */
class TCompteUtilisateurController extends AbstractController
{
    /**
     * @Route("/", name="t_compte_utilisateur_index", methods={"GET"})
     */
    public function index(EntityManagerInterface $entityManager): Response
    {
        $tCompteUtilisateurs = $entityManager
            ->getRepository(TCompteUtilisateur::class)
            ->findAll();

        return $this->render('t_compte_utilisateur/index.html.twig', [
            't_compte_utilisateurs' => $tCompteUtilisateurs,
        ]);
    }

    /**
     * @Route("/new", name="t_compte_utilisateur_new", methods={"GET", "POST"})
     */
    public function new(Request $request, EntityManagerInterface $entityManager): Response
    {
        $tCompteUtilisateur = new TCompteUtilisateur();
        $form = $this->createForm(TCompteUtilisateurType::class, $tCompteUtilisateur);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($tCompteUtilisateur);
            $entityManager->flush();

            return $this->redirectToRoute('t_compte_utilisateur_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('t_compte_utilisateur/new.html.twig', [
            't_compte_utilisateur' => $tCompteUtilisateur,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="t_compte_utilisateur_show", methods={"GET"})
     */
    public function show(TCompteUtilisateur $tCompteUtilisateur): Response
    {
        return $this->render('t_compte_utilisateur/show.html.twig', [
            't_compte_utilisateur' => $tCompteUtilisateur,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="t_compte_utilisateur_edit", methods={"GET", "POST"})
     */
    public function edit(Request $request, TCompteUtilisateur $tCompteUtilisateur, EntityManagerInterface $entityManager): Response
    {
        $form = $this->createForm(TCompteUtilisateurType::class, $tCompteUtilisateur);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('t_compte_utilisateur_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('t_compte_utilisateur/edit.html.twig', [
            't_compte_utilisateur' => $tCompteUtilisateur,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="t_compte_utilisateur_delete", methods={"POST"})
     */
    public function delete(Request $request, TCompteUtilisateur $tCompteUtilisateur, EntityManagerInterface $entityManager): Response
    {
        if ($this->isCsrfTokenValid('delete' . $tCompteUtilisateur->getId(), $request->request->get('_token'))) {
            $entityManager->remove($tCompteUtilisateur);
            $entityManager->flush();
        }

        return $this->redirectToRoute('t_compte_utilisateur_index', [], Response::HTTP_SEE_OTHER);
    }
}
