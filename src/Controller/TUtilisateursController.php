<?php

namespace App\Controller;

use App\Entity\TUtilisateurs;
use App\Form\TUtilisateursType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/t/utilisateurs")
 */
class TUtilisateursController extends AbstractController
{
    /**
     * @Route("/", name="t_utilisateurs_index", methods={"GET"})
     */
    public function index(EntityManagerInterface $entityManager): Response
    {
        $tUtilisateurs = $entityManager
            ->getRepository(TUtilisateurs::class)
            ->findAll();

        return $this->render('t_utilisateurs/index.html.twig', [
            't_utilisateurs' => $tUtilisateurs,
        ]);
    }

    /**
     * @Route("/new", name="t_utilisateurs_new", methods={"GET", "POST"})
     */
    public function new(Request $request, EntityManagerInterface $entityManager): Response
    {
        $tUtilisateur = new TUtilisateurs();
        $form = $this->createForm(TUtilisateursType::class, $tUtilisateur);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($tUtilisateur);
            $entityManager->flush();

            return $this->redirectToRoute('t_utilisateurs_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('t_utilisateurs/new.html.twig', [
            't_utilisateur' => $tUtilisateur,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="t_utilisateurs_show", methods={"GET"})
     */
    public function show(TUtilisateurs $tUtilisateur): Response
    {
        return $this->render('t_utilisateurs/show.html.twig', [
            't_utilisateur' => $tUtilisateur,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="t_utilisateurs_edit", methods={"GET", "POST"})
     */
    public function edit(Request $request, TUtilisateurs $tUtilisateur, EntityManagerInterface $entityManager): Response
    {
        $form = $this->createForm(TUtilisateursType::class, $tUtilisateur);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('t_utilisateurs_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('t_utilisateurs/edit.html.twig', [
            't_utilisateur' => $tUtilisateur,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="t_utilisateurs_delete", methods={"POST"})
     */
    public function delete(Request $request, TUtilisateurs $tUtilisateur, EntityManagerInterface $entityManager): Response
    {
        if ($this->isCsrfTokenValid('delete'.$tUtilisateur->getId(), $request->request->get('_token'))) {
            $entityManager->remove($tUtilisateur);
            $entityManager->flush();
        }

        return $this->redirectToRoute('t_utilisateurs_index', [], Response::HTTP_SEE_OTHER);
    }
}
