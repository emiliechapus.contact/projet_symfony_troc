<?php

namespace App\Controller;

use App\Entity\TObjet;
use App\Form\TObjetType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/t/objet")
 */
class TObjetController extends AbstractController
{
    /**
     * @Route("/", name="t_objet_index", methods={"GET"})
     */
    public function index(EntityManagerInterface $entityManager): Response
    {
        $tObjets = $entityManager
            ->getRepository(TObjet::class)
            ->findAll();

        return $this->render('t_objet/index.html.twig', [
            't_objets' => $tObjets,
        ]);
    }

    /**
     * @Route("/new", name="t_objet_new", methods={"GET", "POST"})
     */
    public function new(Request $request, EntityManagerInterface $entityManager): Response
    {
        $tObjet = new TObjet();
        $form = $this->createForm(TObjetType::class, $tObjet);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($tObjet);
            $entityManager->flush();

            return $this->redirectToRoute('t_objet_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('t_objet/new.html.twig', [
            't_objet' => $tObjet,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="t_objet_show", methods={"GET"})
     */
    public function show(TObjet $tObjet): Response
    {
        return $this->render('t_objet/show.html.twig', [
            't_objet' => $tObjet,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="t_objet_edit", methods={"GET", "POST"})
     */
    public function edit(Request $request, TObjet $tObjet, EntityManagerInterface $entityManager): Response
    {
        $form = $this->createForm(TObjetType::class, $tObjet);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('t_objet_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('t_objet/edit.html.twig', [
            't_objet' => $tObjet,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="t_objet_delete", methods={"POST"})
     */
    public function delete(Request $request, TObjet $tObjet, EntityManagerInterface $entityManager): Response
    {
        if ($this->isCsrfTokenValid('delete'.$tObjet->getId(), $request->request->get('_token'))) {
            $entityManager->remove($tObjet);
            $entityManager->flush();
        }

        return $this->redirectToRoute('t_objet_index', [], Response::HTTP_SEE_OTHER);
    }
}
