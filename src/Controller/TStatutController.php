<?php

namespace App\Controller;

use App\Entity\TStatut;
use App\Form\TStatutType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/t/statut")
 */
class TStatutController extends AbstractController
{
    /**
     * @Route("/", name="t_statut_index", methods={"GET"})
     */
    public function index(EntityManagerInterface $entityManager): Response
    {
        $tStatuts = $entityManager
            ->getRepository(TStatut::class)
            ->findAll();

        return $this->render('t_statut/index.html.twig', [
            't_statuts' => $tStatuts,
        ]);
    }

    /**
     * @Route("/new", name="t_statut_new", methods={"GET", "POST"})
     */
    public function new(Request $request, EntityManagerInterface $entityManager): Response
    {
        $tStatut = new TStatut();
        $form = $this->createForm(TStatutType::class, $tStatut);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($tStatut);
            $entityManager->flush();

            return $this->redirectToRoute('t_statut_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('t_statut/new.html.twig', [
            't_statut' => $tStatut,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="t_statut_show", methods={"GET"})
     */
    public function show(TStatut $tStatut): Response
    {
        return $this->render('t_statut/show.html.twig', [
            't_statut' => $tStatut,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="t_statut_edit", methods={"GET", "POST"})
     */
    public function edit(Request $request, TStatut $tStatut, EntityManagerInterface $entityManager): Response
    {
        $form = $this->createForm(TStatutType::class, $tStatut);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('t_statut_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('t_statut/edit.html.twig', [
            't_statut' => $tStatut,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="t_statut_delete", methods={"POST"})
     */
    public function delete(Request $request, TStatut $tStatut, EntityManagerInterface $entityManager): Response
    {
        if ($this->isCsrfTokenValid('delete'.$tStatut->getId(), $request->request->get('_token'))) {
            $entityManager->remove($tStatut);
            $entityManager->flush();
        }

        return $this->redirectToRoute('t_statut_index', [], Response::HTTP_SEE_OTHER);
    }
}
