<?php

namespace App\Controller;

use App\Entity\TCaracteristiquesObjet;
use App\Form\TCaracteristiquesObjetType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/t/caracteristiques/objet")
 */
class TCaracteristiquesObjetController extends AbstractController
{
    /**
     * @Route("/", name="t_caracteristiques_objet_index", methods={"GET"})
     */
    public function index(EntityManagerInterface $entityManager): Response
    {
        $tCaracteristiquesObjets = $entityManager
            ->getRepository(TCaracteristiquesObjet::class)
            ->findAll();

        return $this->render('t_caracteristiques_objet/index.html.twig', [
            't_caracteristiques_objets' => $tCaracteristiquesObjets,
        ]);
    }

    /**
     * @Route("/new", name="t_caracteristiques_objet_new", methods={"GET", "POST"})
     */
    public function new(Request $request, EntityManagerInterface $entityManager): Response
    {
        $tCaracteristiquesObjet = new TCaracteristiquesObjet();
        $form = $this->createForm(TCaracteristiquesObjetType::class, $tCaracteristiquesObjet);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($tCaracteristiquesObjet);
            $entityManager->flush();

            return $this->redirectToRoute('t_caracteristiques_objet_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('t_caracteristiques_objet/new.html.twig', [
            't_caracteristiques_objet' => $tCaracteristiquesObjet,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="t_caracteristiques_objet_show", methods={"GET"})
     */
    public function show(TCaracteristiquesObjet $tCaracteristiquesObjet): Response
    {
        return $this->render('t_caracteristiques_objet/show.html.twig', [
            't_caracteristiques_objet' => $tCaracteristiquesObjet,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="t_caracteristiques_objet_edit", methods={"GET", "POST"})
     */
    public function edit(Request $request, TCaracteristiquesObjet $tCaracteristiquesObjet, EntityManagerInterface $entityManager): Response
    {
        $form = $this->createForm(TCaracteristiquesObjetType::class, $tCaracteristiquesObjet);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('t_caracteristiques_objet_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('t_caracteristiques_objet/edit.html.twig', [
            't_caracteristiques_objet' => $tCaracteristiquesObjet,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="t_caracteristiques_objet_delete", methods={"POST"})
     */
    public function delete(Request $request, TCaracteristiquesObjet $tCaracteristiquesObjet, EntityManagerInterface $entityManager): Response
    {
        if ($this->isCsrfTokenValid('delete'.$tCaracteristiquesObjet->getId(), $request->request->get('_token'))) {
            $entityManager->remove($tCaracteristiquesObjet);
            $entityManager->flush();
        }

        return $this->redirectToRoute('t_caracteristiques_objet_index', [], Response::HTTP_SEE_OTHER);
    }
}
