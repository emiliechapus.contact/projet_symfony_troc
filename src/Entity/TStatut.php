<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * TStatut
 *
 * @ORM\Table(name="t_statut", uniqueConstraints={@ORM\UniqueConstraint(name="label_UNIQUE", columns={"label"})})
 * @ORM\Entity
 */
class TStatut
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private ?int $id;

    /**
     * @var string
     *
     * @ORM\Column(
     *  name="label",
     *  type="string",
     *  length=50,
     *  nullable=false,
     *  options={"comment"="lenght>1"}
     * )
     * @Assert\Unique,
     */
    private $label = '';

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLabel(): ?string
    {
        return $this->label;
    }

    public function setLabel(string $label): self
    {
        $this->label = filter_var($label, FILTER_SANITIZE_STRING);

        return $this;
    }
}
