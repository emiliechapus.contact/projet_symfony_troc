<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * TKeys
 *
 * @ORM\Table(
 *  name="t_keys",
 *  uniqueConstraints={@ORM\UniqueConstraint(name="name_UNIQUE", columns={"name"})}
 * )
 * 
 * @ORM\Entity
 */
class TKeys
{
    /**
     * @var int
     *
     * @ORM\Column(
     *  name="keys_id",
     *  type="integer",
     *  nullable=false
     * )
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private ?int $keysId;

    /**
     * @var string
     *
     * @ORM\Column(
     *  name="name",
     *  type="string",
     *  length=250,
     *  nullable=false,
     *  options={"comment"="lenght>1"}
     * )
     * 
     * @Assert\Length(
     *  min = 2,
     *  max = 250,
     *  minMessage = "Le nom doit au minimum être composé de {{ limit }} caractères.",
     *  maxMessage = "Le nom doit au minimum être composé de {{ limit }} caractères",
     */
    private string $name;

    public function getKeysId(): ?int
    {
        return $this->keysId;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        // $this->name = filter_var($name, FILTER_SANITIZE_STRING);
        $this->name = htmlspecialchars($name);


        return $this;
    }
}
