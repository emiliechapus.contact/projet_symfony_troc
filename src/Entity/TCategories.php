<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * TCategories
 *
 * @ORM\Table(
 *  name="t_categories",
 *  uniqueConstraints={@ORM\UniqueConstraint(name="name_UNIQUE", columns={"name"})}
 * )
 * @ORM\Entity
 */
class TCategories
{
    /**
     * @var int
     *
     * @ORM\Column(
     *  name="id",
     *  type="integer",
     *  nullable=false
     * )
     * 
     * @ORM\Id
     * 
     * @ORM\GeneratedValue(
     *  strategy="IDENTITY"
     * )
     */
    private ?int $id;

    /**
     * @var string
     *
     * @ORM\Column(
     *  name="name",
     *  type="string",
     *  length=50,
     *  nullable=false,
     *  options={"comment"="lenght>1"}
     * )
     * 
     * @Assert\Length(
     *      min = 2,
     *      max = 50,
     *      minMessage = "Votre nom doit contenir au minimum {{ limit }} caractères.",
     *      maxMessage = "Votre nom doit contenir au maximum {{ limit }} caractères."
     * )
     */
    private string $name = '';

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function __toString()

    {
        return strval($this->name);
    }
}
