<?php

namespace App\Entity;

use App\Entity\TCategories;
use App\Entity\TUtilisateurs;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * TObjet
 *
 * @ORM\Table(name="t_objet", indexes={@ORM\Index(name="fk_category_idx", columns={"id"}), @ORM\Index(name="fk_category_idx1", columns={"fk_category"}), @ORM\Index(name="fk_user_id_idx", columns={"fk_user_id"})})
 * @ORM\Entity
 */
class TObjet
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private ?int $id;

    /**
     * @var string
     *
     * @ORM\Column(
     *  name="name",
     *  type="string",
     *  length=200,
     *  nullable=false,
     *  options={"comment"="lenght >1"}
     * )
     * 
     * @Assert\Length(
     *   min = 2,
     *   max = 200,
     *   minMessage = "Le nom de votre objet doit au minimum contenir {{ limit }} caractères.",
     *   maxMessage = "Le nom de votre objet doit au maximum contenir {{ limit }} caractères."
     * )
     * 
     */
    private string $name = '';

    /**
     * @var string
     *
     * @ORM\Column(
     *  name="description",
     *  type="text",
     *  length=65535,
     *  nullable=false,
     *  options={"comment"="lenght>10"}
     * )
     * 
     * @Assert\Length(
     *   min = 11,
     *   max = 65535,
     *   minMessage = "Votre description doit au minimum contenir {{ limit }} caractères.",
     *   maxMessage = "Votre description doit au maximum contenir {{ limit }} caractères."
     * )
     * 
     */
    private string $description = '';

    /**
     * @var string
     *
     * @ORM\Column(
     *  name="illustration",
     *  type="string",
     *  length=255,
     *  nullable=false,
     *  options={"comment"="lenght>15"}
     * )
     * 
     * @Assert\Length(
     *   min = 15,
     *   max = 255,
     *   minMessage = "l'url de votre image doit au minimum contenir {{ limit }} caractères.",
     *   maxMessage = "l'url de votre image doit au maximum contenir {{ limit }} caractères."
     * )
     * 
     * @Assert\Url
     * 
     */
    private string $illustration = '';

    /**
     * @var \TCategories
     *
     * @ORM\ManyToOne(targetEntity="TCategories")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="fk_category", referencedColumnName="id")
     * })
     * @Assert\Type("App\Entity\TCategories")
     */
    private $fkCategory;

    /**
     * @var \TUtilisateurs
     *
     * @ORM\ManyToOne(targetEntity="TUtilisateurs")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="fk_user_id", referencedColumnName="user_id")
     * })
     * @Assert\Type("App\Entity\TUtilisateurs")
     */
    private $fkUser;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="TMessage", inversedBy="idObjet")
     * @ORM\JoinTable(name="t_message_objets",
     *   joinColumns={
     *     @ORM\JoinColumn(name="id_objet", referencedColumnName="id")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="id_message", referencedColumnName="message_id")
     *   }
     * )
     * @Assert\Type("\Doctrine\Common\Collections\Collection")
     */
    private $idMessage;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->idMessage = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = filter_var($name, FILTER_SANITIZE_STRING);

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = filter_var($description, FILTER_SANITIZE_STRING);

        return $this;
    }

    public function getIllustration(): ?string
    {
        return $this->illustration;
    }

    public function setIllustration(string $illustration): self
    {
        $this->illustration = filter_var($illustration, FILTER_SANITIZE_STRING);

        return $this;
    }

    public function getFkCategory(): ?TCategories
    {
        return $this->fkCategory;
    }

    public function setFkCategory(?TCategories $fkCategory): self
    {
        $this->fkCategory = $fkCategory;

        return $this;
    }

    public function getFkUser(): ?TUtilisateurs
    {
        return $this->fkUser;
    }

    public function setFkUser(?TUtilisateurs $fkUser): self
    {
        $this->fkUser = $fkUser;

        return $this;
    }

    /**
     * @return Collection|TMessage[]
     */
    public function getIdMessage(): Collection
    {
        return $this->idMessage;
    }

    public function addIdMessage(TMessage $idMessage): self
    {
        if (!$this->idMessage->contains($idMessage)) {
            $this->idMessage[] = $idMessage;
        }

        return $this;
    }

    public function removeIdMessage(TMessage $idMessage): self
    {
        $this->idMessage->removeElement($idMessage);

        return $this;
    }
}
