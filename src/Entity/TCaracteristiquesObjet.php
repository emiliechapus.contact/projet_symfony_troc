<?php

namespace App\Entity;

use App\Entity\TKeys;
use App\Entity\TObjet;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * TCaracteristiquesObjet
 *
 * @ORM\Table(
 *  name="t_caracteristiques_objet",
 *  indexes={
 *      @ORM\Index(
 *          name="fk_key_idx",
 *          columns={"key"}
 *      ),
 *      @ORM\Index(
 *          name="fk_objet_id_idx",
 *          columns={"object_id"}
 *      )
 *  }
 * )
 * 
 * @ORM\Entity
 */
class TCaracteristiquesObjet
{

    /**
     * @var int
     *
     * @ORM\Column(name="object_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private ?int $id;

    /**
     * @var int
     *
     * @ORM\Column(
     *  name="value",
     *  type="integer",
     *  nullable=false,
     *  options={"comment"="lenght>0"}
     *  )
     * 
     * @Assert\GreaterThan(0)
     * 
     */
    private int $value;

    /**
     * @var \TKeys
     *
     * @ORM\ManyToOne(
     *  targetEntity="TKeys"
     * )
     * 
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(
     *      name="key",
     *      referencedColumnName="keys_id"
     *  )
     * })
     * @Assert\Type("App\Entity\TKeys")
     * 
     */
    private $key;

    /**
     * @var \TObjet
     *
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="TObjet")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(
     *      name="object_id",
     *      referencedColumnName="id")
     * })
     * @Assert\Type("App\Entity\TObjet")
     */
    private $object;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getValue(): ?int
    {
        return $this->value;
    }

    public function setValue(int $value): self
    {
        $this->value = $value;

        return $this;
    }

    public function getKey(): ?TKeys
    {
        return $this->key;
    }

    public function setKey(?TKeys $key): self
    {
        $this->key = $key;

        return $this;
    }

    public function getObject(): ?TObjet
    {
        return $this->object;
    }

    public function setObject(?TObjet $object): self
    {
        $this->object = $object;

        return $this;
    }
}
