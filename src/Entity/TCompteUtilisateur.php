<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * TCompteUtilisateur
 *
 * @ORM\Table(
 *  name="t_compte_utilisateur",
 *  uniqueConstraints={
 *      @ORM\UniqueConstraint(
 *          name="login_UNIQUE",
 *          columns={"login"}
 *      )
 *  }
 * )
 * @ORM\Entity
 */
class TCompteUtilisateur
{
    /**
     * @var int
     *
     * @ORM\Column(
     *  name="id",
     *  type="integer",
     *  nullable=false
     *  )
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private ?int $id = null;

    /**
     * @var string
     *
     * @ORM\Column(
     *  name="login",
     *  type="string",
     *  length=45,
     *  nullable=false,
     *  options={"default"="> 7 characters"}
     * )
     * 
     * @Assert\Length(
     *  min = 2,
     *  max = 45,
     *  minMessage = "Votre login doit au minimum être composé de {{ limit }} caractères.",
     *  maxMessage = "Votre login doit au minimum être composé de {{ limit }} caractères",
     * )
     */
    private string $login = '';

    /**
     * @var string
     *
     * @ORM\Column(
     *  name="password",
     *  type="string",
     *  length=45,
     *  nullable=false,
     *  options={"default"=">7 characters AND regex(password)"}
     * )
     * 
     * @Assert\Regex(
     *  pattern = "/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[a-zA-Z]).{8,}$/",
     *  message = "Votre mot de passe de passe n'est pas valide.",
     * )
     * 
     * @Assert\Length(
     *  min = 8,
     *  max = 45,
     *  minMessage = "Votre mot de passe doit au minimum être composé de {{ limit }} caractères.",
     *  maxMessage = "Votre mot de passe doit au minimum être composé de {{ limit }} caractères"
     * )
     */
    private string $password = '';

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLogin(): ?string
    {
        return $this->login;
    }

    public function setLogin(string $login): self
    {
        $this->login = $login;

        return $this;
    }

    public function __toString()

    {

        return strval($this->login);
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }
}
